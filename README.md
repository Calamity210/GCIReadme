# SCoRe Mobile App

## Screenshots
![Screenshots](https://i.imgur.com/0eun6d6m.jpg)  ![Screenshots](https://i.imgur.com/FnnJr2Bm.jpg)  ![Screenshots](https://i.imgur.com/Lyv0Bl3m.jpg)

## Overview
The Sustainable Computing Research Group(SCoRe) has conducted research covering various aspects of sensor networks, embeded systems, digital forensic, information security, mobile applications, cloud blockchain and software tools.
The goal of our research is to generate computing solutions through identifying low cost methodologies and strategies leading to sustainability.

Currently, the SCoRe group is at a stage of its evolution in which it has been able to secure high donor confidence as proven by many foreign funded projects.

We at SCoRe produce sustainable computing solutions. We have developed several affordable and sustainable ICT solutions specially in regards to the requirements in developing regions.

This respository contains our app, SCoRe Mobile App, which gives you an overall summary of what we do, links to more of our succesful projects, our website, github, email, gitter and so on. More or less everything we do is compiled in a single place!

## How to install
If you actually bothered to read through that all, you are probably wanting to install our app, right? Hopefully you do want to know how, so I'll tell you:
  - 1. Fork this repository
    - Forking a repository allows you to freely mess around with a project without affecting the original, basically a copy!
    
    ![Screenshots](https://i.imgur.com/p31LWH7.png)
  - 2. Go to the forked repository, tap the green *clone and download* button and copy the link given to you.
    - This link will be used to clone the project on to your PC in the following steps.
  
    ![Screenshots](https://i.imgur.com/FuXb2Lr.png)
  - 3. Open up a terminal and go to your preferred directory using 'cd [dir]'.
    - This is where you will clone the project to.
    
    ![Screenshots](https://i.imgur.com/A0qurvR.png)
  - 4. In the terminal type "git clone [url you copied]"
    
    ![Screenshots](https://i.imgur.com/38PJ0Lg.png)
  - 5. The project is now on your PC and you can open it using the IDE of your Preference!
  
  - 6. For the purpose of this tutorial I will be using Android Studio. Launch it.
  
  - 7. Tap on open an existing Android Studio project, locate the folder you cloned in, and launch it
  
    ![Screenshots](https://i.imgur.com/9E4brPW.png)
  - 8. Wait for the project to finish syncing
  
    ![Screenshots](https://i.imgur.com/bECiDJB.png)
    
  - 9. Once the sync is finished, plug in your phone to your PC
    - Make sure USB Debugging is enabled, heres how:       https://www.embarcadero.com/starthere/xe5/mobdevsetup/android/en/enabling_usb_debugging_on_an_android_device.html
  
  - 10. Press the green play button, this will start building the app on to your phone.
    - The app will appear on your phone once it is done building
    
    ![Screenshots](https://i.imgur.com/lndWdhA.png)
  - 11. Thats it!
  
  
  
  ## Wow! Props to you for actually making it down here :)
    
